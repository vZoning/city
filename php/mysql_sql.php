<?php
$city = json_decode(file_get_contents('../city.json'));

$table_name = '`citys`';
$sql_file = '../sql/citys.mysql.sql';
$time = date('Y-m-d H:i:s');
$top = <<<EOF
/*
 Server Type : MySQL
 Date : $time
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


DROP TABLE IF EXISTS $table_name;
CREATE TABLE $table_name  (
  `id` int(7) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行政区划id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '行政区划名',
  `pid` int(7) UNSIGNED NOT NULL COMMENT '行政区划父级id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pid` (`pid`),
  KEY `name` (`name`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

\n
EOF;
file_put_contents($sql_file,$top);

$i = 1;
$val = [];
foreach($city as $v){
    $val[] = "({$v->id},'{$v->name}',{$v->pid})";
    if($i === 20){
        $i = 1;
        $sql = 'insert into '.$table_name.'(`id`,`name`,`pid`) values ';
        $sql .= implode(',',$val);
        $sql .= ';'.PHP_EOL;
        $val = [];
        file_put_contents($sql_file,$sql,FILE_APPEND);
    }
    $i++;
}
echo $sql_file.'生成完毕';